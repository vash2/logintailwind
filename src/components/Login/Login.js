import React, { useState } from "react";
import image from "../../img/user4.png";
import validate from "./validateInfo";
import useFormLogin from "./useForm";
import { useHistory } from "react-router-dom";

const Login = ({ submitForm }) => {
  const { handleChange, handleSubmit, values, errors } = useFormLogin(
    submitForm,
    validate
  );
  const history = useHistory();
  function submitForm() {
    history.push("/");
  }

  return (
    <div className="flex overflow-hidden justify-center items-center w-screen h-screen bg-gradient-to-r from-red-300 to-green-300">
      <div className="flex sm:flex-row flex-col sm:w-2/3 w-5/6 sm:h-auto sm:justify-center">
        <div className="flex flex-col rounded-l-lg justify-center sm:w-2/6 w-full bg-gradient-to-r from-teal-400 to-blue-500 shadow sm:shadow-md md:shadow-lg lg:shadow-xl xl:shadow-2xl ">
          <h2 className="w-full text-white text-center text-3xl font-bold tracking-wider mt-10">
            Login Form
          </h2>
          <div className="w-full flex justify-center items-center my-2">
            <img
              className="sm:w-2/3 w-1/3 border-2 border-gray-300 rounded-full p-2 border-opacity-75"
              src={image}
              alt="spaceship"
            />
          </div>
          <div className="flex flex-col h-full text-white bg-gray-lighter items-center">
            <a href="#">Create account</a>
          </div>
        </div>

        <div className="sm:w-3/6 w-full shadow sm:shadow-md md:shadow-lg lg:shadow-xl xl:shadow-2xl">
          <form
            onSubmit={handleSubmit}
            className="bg-white px-8 pt-6 rounded-r-lg pb-8 "
            noValidate
          >
            <h2 className="w-full text-center text-3xl font-bold tracking-wider mb-10">
              Login
            </h2>
            <div className="mb-4">
              <label className="block text-gray-700 text-sm font-bold mb-2">
                Username
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
                type="text"
                placeholder="Username"
                value={values.username}
                name="username"
                onChange={handleChange}
              />

              {errors.username && <p className="text-red-400">{errors.username}</p>}
            </div>
            <div className="mb-6">
              <label className="block text-gray-700 text-sm font-bold mb-2">
                Password
              </label>
              <input
                className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 mb-3 leading-tight focus:outline-none focus:shadow-outline"
                id="password"
                type="password"
                placeholder="******************"
                value={values.password}
                name="password"
                onChange={handleChange}
              />
              {errors.password && <p className="text-red-400">{errors.password}</p>}

              {/* <p class="text-red-500 text-xs italic">
                Please choose a password.
              </p> */}
            </div>
            <div className="flex items-center justify-between">
              <button
                type="submit"
                className="bg-gradient-to-r from-blue-400 to-blue-400 hover:from-pink-500 hover:to-orange-500 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline focus:border-blue-300 ..."
              >
                Sign In
              </button>
              <a
                className="inline-block align-baseline font-bold text-sm 
                text-blue-500 hover:text-blue-800 "
                href="/"
              >
                Forgot Password?
              </a>
            </div>
            <p className="text-center text-gray-500 text-xs pt-5"></p>
          </form>
        </div>
      </div>
    </div>
  );
};
export default Login;
